import logging
import os
import urllib
import urllib.request
import uuid

import cv2
import py_eureka_client.eureka_client as eureka_client
from flask import Flask, request, Response
from moviepy.video.io.VideoFileClip import VideoFileClip

import utils

app = Flask(__name__)
log = logging
log.basicConfig(level=logging.DEBUG)
eureka_client.init_registry_client(eureka_server=utils.EUREKA_SERVER,
                                   app_name=utils.APP_NAME,
                                   instance_port=utils.INSTANCE_PORT)


@app.route('/footage/create', methods=["POST"])
def save_footage():
    try:

        req_data = request.get_json()

        article_link = str(req_data["articleLink"])
        duration = float(req_data["duration"])
        screenshot_access_key = req_data["screenshotAccessKey"]
        video_path = req_data["videoPath"]
        temp_video = str(uuid.uuid1()) + ".mp4"

        screenshot = {'access_key': screenshot_access_key, 'full_page': 'true', 'scroll_page': 'true',
                      'url': article_link}

        screenshot_file_name = str(uuid.uuid1()) + ".jpg"

        urllib.request.urlretrieve("https://api.apiflash.com/v1/urltoimage?" + urllib.parse.urlencode(screenshot),
                                   screenshot_file_name)

        image = cv2.imread(screenshot_file_name)

        height, width, channels = image.shape

        crop_height = int(width * 9 / 16)

        total = height - crop_height

        out = cv2.VideoWriter(temp_video, cv2.VideoWriter_fourcc(*'MP4V'), 24, (width, crop_height))

        for i in range(total):
            crop_img = image[i:i + crop_height, 0:width]
            out.write(crop_img)

        out.release()

        os.remove(screenshot_file_name)

        clip = VideoFileClip(temp_video)

        speedup = duration / clip.duration

        command = 'ffmpeg -i ' + temp_video + ' -filter:v "setpts=' + str(speedup) + '*PTS" ' + video_path

        os.system(command)

        return Response(status=200)
    except Exception as e:
        return Response(str(e), status=400)


if __name__ == '__main__':
    app.run()
